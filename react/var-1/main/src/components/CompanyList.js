import React, { Component } from 'react'
import CompanyStore from '../stores/CompanyStore'
import Company from './Company'

class CompanyList extends Component {
	constructor(){
		super()
		this.state = {
			companies : []
		}
		
	}
	componentDidMount(){
		this.store = new CompanyStore()
		this.setState({
			companies : this.store.getAll()
		})
		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				companies : this.store.getAll()
			})			
		})
	}
  render() {
    return (
      <div>

      </div>
    )
  }
}

// # Subiect 4
// # Tematica: REACT

// # Avand urmatoarea aplicatie create folosind `create-react-app`, modificati `Company` astfel incat:
// - aplicatia se deseneaza corect (0.5 pts);
// - `CompanyList` se deseneaza ca o lista de  `Company`, iar fiecare `Company` are un buton cu eticheta `edit` (0.5 pts);
// - Daca se da click pe butonul edit al unui `Company` aceasta trece in mod de editare (0.5 pts);
// - Daca in mod edit se da click pe butonul cancel al unui `Company` aceasta trece in mod view (0.5 pts);
// - Se poate salva o companie, iar schimbarea se reflecta in lista de companii (0.5 pts);

export default CompanyList

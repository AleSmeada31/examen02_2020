import {EventEmitter} from 'fbemitter'

class CompanyStore{
	constructor(){
		this.companies = [{
			id : 1,
			name : 'acme inc',
			employees : 100,
			revenue : 1000
		},{
			id : 2,
			name : 'apex llc',
			employees : 20,
			revenue : 100
		}]
		this.emitter = new EventEmitter()
	}
	addOne(company){
		this.companies.push(company)
		this.emitter.emit('UPDATE')
	}
	getAll(){
		return this.companies
	}
	deleteOne(id){
		let index = this.companies.findIndex((e) => e.id === id)
		if (index !== -1){
			this.companies.splice(index, 1)
		}
		this.emitter.emit('UPDATE')
	}
	saveOne(id, company){
		let index = this.companies.findIndex((e) => e.id === id)
		if (index !== -1){
			Object.assign(this.companies[index], company) 
		}
		this.emitter.emit('UPDATE')
	}

}

// # Subiect 4
// # Tematica: REACT

// # Avand urmatoarea aplicatie create folosind `create-react-app`, modificati `Company` astfel incat:
// - aplicatia se deseneaza corect (0.5 pts);
// - `CompanyList` se deseneaza ca o lista de  `Company`, iar fiecare `Company` are un buton cu eticheta `edit` (0.5 pts);
// - Daca se da click pe butonul edit al unui `Company` aceasta trece in mod de editare (0.5 pts);
// - Daca in mod edit se da click pe butonul cancel al unui `Company` aceasta trece in mod view (0.5 pts);
// - Se poate salva o companie, iar schimbarea se reflecta in lista de companii (0.5 pts);


export default CompanyStore